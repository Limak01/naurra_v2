/* IMPORTS */
#include <gtk/gtk.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// Global variables
GtkWidget* window;
GtkBuilder* user_interface;
GtkCssProvider* css_provider;

// Link css file with application
static void init_css() {
    css_provider = gtk_css_provider_new();
    gtk_css_provider_load_from_path(css_provider, "styles.css", NULL);
    gtk_style_context_add_provider_for_screen(gdk_screen_get_default(), GTK_STYLE_PROVIDER(css_provider), GTK_STYLE_PROVIDER_PRIORITY_USER);
}

// Link application ui 
static void init_interface() {
    user_interface = gtk_builder_new();
    gtk_builder_add_from_file(user_interface, "ui.glade", NULL);
}

// Close app
void close_app() {
    gtk_main_quit();
}

// Logout
void log_out() {
    system("killall xmonad-x86_64-linux");
}

// Poweroff
void poweroff() {
    system("shutdown -h now");
}

// Reboot
void reboot() {
    system("reboot");
}

void get_uptime() {
    // Get uptime labe from interface
    GtkLabel* label = GTK_LABEL(gtk_builder_get_object(user_interface, "uptime_label"));

    // Execute uptime command and read output from it
    FILE* uptime = popen("uptime -p", "r");
    char buffer[100];

    if(uptime == NULL) {
        fputs("POPEN: failed to execute command.", stderr); 
    } else {
        fgets(buffer, 100, uptime); // Get command output and put it in the buffer
    }
    
    pclose(uptime); // Close stream
    
    // fputs might add new line so this function deletes it
    strtok(buffer, "\n");

    // Set label text to output from uptime command
    gtk_label_set_text(label, buffer);
}

// Suspend
void suspend() {
    system("systemctl suspend");
    gtk_main_quit();
}

// Hibernate
void hibernate() {
    system("systemctl hibernate");
    gtk_main_quit();
}

// Lock using "slock"
void lock() {
    system("slock");
    gtk_main_quit();
}

int main(int argc, char** argv) {
    
    gtk_init(&argc, &argv);
   
    init_css(); // Call function to load css file
    init_interface(); // Call function to load interface file
    get_uptime(); // Call function to get current uptime

    // Get window from ui.glade interface
    window = GTK_WIDGET(gtk_builder_get_object(user_interface, "window"));
    
    // Connect all signals specified with ui.glade
    gtk_builder_connect_signals(user_interface, NULL);
    g_object_unref(user_interface);
    
    // Render everything
    gtk_widget_show_all(window);

    // Start main loop
    gtk_main();

    return 0; 
}
