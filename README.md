![app screenshot](img/screen.png)

# Naurra v2

Simple GTK logout application written in C. Better version of [naurra](https://gitlab.com/Limak01/naurra) with icons.
